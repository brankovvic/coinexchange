
 (function() {

 	var App = {};

 	App.Sentinel = {

 		init: function() {

			// Scope Helper
			this._bind = function(fn, me) {
				return function() {
					return fn.apply(me, arguments);
				};
			};

			// Safety for transitions
			$(document.body).removeClass('preload');

			// Add event listeners
			this.addListeners();

			// Validate forms
			$('.validate-form').bootstrapValidator();

		},

		addListeners: function() {

		},

		handleSomeEvent: function () {

		},

	};

	$(function() {

		return App.Sentinel.init();

	});

}).call(this);
