jQuery(document).ready(function()
{
	$.get('http://api.bootswatch.com/3/', function(data)
	{
		var themes = data.themes;

		var select = $('.bootswatch-select');

		themes.forEach(function(value, index)
		{
			select.append(
				'<li><a href="#" data-theme="' + index + '">' + value.name + '</li>'
			);
		});

		select.on('click', 'a', function(e)
		{
			e.preventDefault();

			var theme = themes[$(this).data('theme')];

			var url = theme === undefined ? null : theme.cssCdn;

			$('.bootswatch-css').attr('href', url);
		}).change();

	}, 'json').fail(function()
	{
		alert('Failed to load bootswatch');
	});
});
