<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Route::get('/buy', 'AdsController@showBuyAds')->name('buy');

Route::get('/sell', 'AdsController@showSellAds')->name('sell');

// Authentication Routes
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'account'], function () {
        Route::get('/', 'AccountController@account')->name('account');
        Route::get('edit', 'AccountController@edit')->name('showEditAccount');
        Route::patch('edit', 'AccountController@update')->name('updateAccount');
        Route::get('trades', 'TradeController@showUserTrades')->name('showUserTrades');
    });

    Route::group(['prefix'=>'dashboard'], function() {
        Route::get('/', 'DashboardController@index')->name('showDashboard');
    });

    // some error on localhost can go with chat TODO solve this issue
    Route::get('chat1','ChatController@index')->name('showChat');

    //Two Factor Routes
    Route::get('two-factor', 'TwoFactorController@showHelp')->name('showTwoFactorHelp');
    Route::post('two-factor/enable', 'TwoFactorController@enableTwoFactor')->name('enableTwoFactor');

    Route::group(['middleware' => 'twoFactorEnabled'], function () {
        Route::get('two-factor/show/confirm', 'TwoFactorController@showConfirmation')->name('showConfirmation');
        Route::post('two-factor/check/confirmation',
            'TwoFactorController@checkConfirmation')->name('checkConfirmation');
        Route::get('two-factor/sms/resend', 'TwoFactorController@resendSms')->name('resendSms');
        Route::get('two-factor/call/request', 'TwoFactorController@requestCall')->name('requestCall');
    });

    Route::group(['prefix' => 'trade'], function () {
        Route::get('/create', 'AdsController@showCreate')->name('showCreate');
        Route::post('/create', 'AdsController@createTrade')->name('createTrade');
    });

    Route::group(['prefix'=>'payment'], function () {
        Route::post('makeDeposit', 'PayPalController@makeDeposit')->name('payPalDeposit');
        Route::get('complete', 'PayPalController@paymentComplete')->name('payPalCompleted');
        Route::get('cancel', 'PayPalController@paymentCancel')->name('payPalCancel');
    });

    Route::group(['prefix'=>'wallet'], function () {
        Route::get('/', 'WalletController@index')->name('wallet');
        Route::post('/send', 'WalletController@transferCoinAmount')->name('transferCoin');
    });

    Route::group(['prefix'=>'trade'], function() {
        Route::get('/{adId}', 'TradeController@showCreateTrade')->where('adId', '[0-9]+')
            ->name('showCreateTrade');
        Route::post('/create/{adId}', 'TradeController@createNewTrade')->where('adId', '[0-9]+')
            ->name('createNewTrade');
    });

    Route::group(['prefix'=>'contract'], function() {
       Route::post('/approve', 'TradeContractController@approveNewContract')->name('approveContract');
    });

    Route::get('showPaymentForm', 'PayPalController@showPayment')->name('depositFounds');
    Route::post('submitPaymentForm', 'PayPalController@makeDeposit')->name('submitPaymentForm');
});
