<?php

return [
    'mode' => 'sandbox',
    'service.EndPoint' => 'https://api.sandbox.paypal.com',
    'http.ConnectionTimeOut' => 30,
    'log.LogEnabled' => true,
    'log.FileName' => storage_path('logs/paypal.log'),
    'log.LogLevel' => 'FINE'
];