<?php
return [
    'btc'=>env('BLOCK_IO_BTC'),
    'litecoin'=>env('BLOCK_IO_LITECOIN'),
    'dogcoin'=>env('BLOCK_IO_DOGECOIN'),
    'pin'=>env('PIN'),
    'version'=>env('version',1),
    'btcBalanceExpire'=>60
];