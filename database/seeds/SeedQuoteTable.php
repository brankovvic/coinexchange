<?php

use Illuminate\Database\Seeder;

class SeedQuoteTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->insert([
            'quote' => '“Dwell on the beauty of life. Watch the stars, and see yourself running with them.”',
            'author' => 'Marcus Aurelius',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('quotes')->insert([
            'quote' => '“Very little is needed to make a happy life; it is all within yourself, in your way of thinking.”',
            'author' => 'Marcus Aurelius',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('quotes')->insert([
            'quote' => '“Everything we hear is an opinion, not a fact. Everything we see is a perspective, not the truth.”',
            'author' => 'Marcus Aurelius',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('quotes')->insert([
            'quote' => '“When you arise in the morning, think of what a precious privilege it is to be alive - to breathe, to think, to enjoy, to love.”',
            'author' => 'Marcus Aurelius',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('quotes')->insert([
            'quote' => '“When you arise in the morning, think of what a precious privilege it is to be alive - to breathe, to think, to enjoy, to love.”',
            'author' => 'Marcus Aurelius',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('quotes')->insert([
            'quote' => '“The object of life is not to be on the side of the majority, but to escape finding oneself in the ranks of the insane.”',
            'author' => 'Marcus Aurelius',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
