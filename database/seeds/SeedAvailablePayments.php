<?php

use Illuminate\Database\Seeder;

class SeedAvailablePayments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('available_payments')->insert([
            'payment_name' => 'PayPal',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
