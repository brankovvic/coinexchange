<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function(Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('fromId')->unsigned()->index();
            $blueprint->integer('toId')->unsigned()->index();
            $blueprint->timestamps();

            $blueprint->foreign('fromId')->references('id')->on('users')->onDelete('cascade');
            $blueprint->foreign('toId')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
