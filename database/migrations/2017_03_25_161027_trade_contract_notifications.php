<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TradeContractNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_contract_notifications', function(Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('tradeContractId')->unsigned();
            $blueprint->tinyInteger('type')->default(0);
            $blueprint->string('message');
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_contract_notifications');
    }
}
