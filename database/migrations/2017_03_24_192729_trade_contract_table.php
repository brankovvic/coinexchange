<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TradeContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_contracts', function(Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('adId')->unsigned()->index();
            $blueprint->integer('userId')->unsigned();
            $blueprint->decimal('price', 8,2);
            $blueprint->decimal('coinPrice', 9,6);
            $blueprint->tinyInteger('status')->default(0);
            $blueprint->timestamps();

            $blueprint->foreign('adId')->references('id')->on('ads')->onDelete('cascade');
            $blueprint->foreign('userId')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_contracts');
    }
}
