<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payments', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('paymentOptionId')->unsigned();
            $blueprint->string('paymentId');
            $blueprint->integer('userId')->unsigned();
            $blueprint->double('amount',8,2)->default(0);
            $blueprint->timestamps();

            $blueprint->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $blueprint->foreign('paymentOptionId')->references('id')->on('available_payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
