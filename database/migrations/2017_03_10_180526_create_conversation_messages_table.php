<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversation_messages', function(Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('conversationId')->unsigned()->index();
            $blueprint->integer('fromId')->unsigned()->index();
            $blueprint->integer('toId')->unsigned()->index();
            $blueprint->text('message');
            $blueprint->timestamps();

            $blueprint->foreign('conversationId')->references('id')->on('conversations')->onDelete('cascade');
            $blueprint->foreign('fromId')->references('id')->on('users')->onDelete('cascade');
            $blueprint->foreign('toId')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
