<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('userId')->unsigned();
            $blueprint->tinyInteger('trade_type')->default(0);
            $blueprint->integer('currencyId')->unsigned();
            $blueprint->decimal('price', 9, 2);
            $blueprint->decimal('lat', 9, 6);
            $blueprint->decimal('lon', 9, 6);
            $blueprint->integer('min_amount');
            $blueprint->integer('max_amount');
            $blueprint->string('description');
            $blueprint->tinyInteger('status')->default(0);
            $blueprint->boolean('authy_verified')->default(false);
            $blueprint->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $blueprint->foreign('currencyId')->references('id')->on('countries')->onDelete('cascade');
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
