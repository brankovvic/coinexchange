<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EscrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escrow', function(Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('tradeContractId')->unsigned()->index();
            $blueprint->decimal('price',9,2)->nullable();
            $blueprint->decimal('coinPrice',9,6)->nullable();
            $blueprint->timestamps();

            $blueprint->foreign('tradeContractId')->references('id')->on('trade_contracts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_contracts');
    }
}
