<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeContract extends Model
{
    protected $fillable = [
        'adId', 'userId', 'price', 'coinPrice'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ad() {
        return $this->hasOne(Ads::class, 'id', 'adId');
    }
}
