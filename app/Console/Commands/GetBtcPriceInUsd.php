<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class GetBtcPriceInUsd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'btcPrice:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets BTC Price And Save The Price In Redis';
    /**
     * @var RedisManager|null
     */
    private $redis = null;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redis = Redis::getFacadeRoot();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = file_get_contents(getenv('MAIN_BITCOIN_PRICE_URL'));
        if ($data === false) return false;
        $data = json_decode($data);
        $this->redis->set('btcPriceInUsd', $data->btc->usd->bitfinex->last);
    }
}
