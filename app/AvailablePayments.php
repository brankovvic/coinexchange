<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailablePayments extends Model
{
    protected $fillable = ['payment_name'];
}
