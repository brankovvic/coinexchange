<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'firstName',
        'lastName',
        'mobile',
        'authId',
        'authOn',
        'countryCode',
        'balance'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads() {
        return $this->hasMany(Ads::class,'userId','id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute() {
        return ucfirst($this->getAttribute('firstName')) ." " . ucfirst($this->getAttribute('lastName'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authenticationHistory() {
        return $this->hasMany(AuthenticationHistory::class,'userId','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|mixed
     */
    public function contactTrades() {
        return $this->hasMany(TradeContract::class,'userId','id');
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments() {
        return $this->hasMany(UserPayment::class,'userId','id');
    }
}
