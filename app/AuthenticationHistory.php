<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AuthenticationHistory extends Model
{
    /**
     * @var string
     */
    public $table = "authenticationHistory";
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'userId',
        'agent',
        'email',
        'ipAddress'
    ];
}
