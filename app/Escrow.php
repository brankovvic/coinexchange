<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escrow extends Model
{
    /**
     * @var string
     */
    protected $table = "escrow";
    /**
     * @var array
     */
    protected $fillable = [
      'tradeContractId', 'price', ''
    ];
}
