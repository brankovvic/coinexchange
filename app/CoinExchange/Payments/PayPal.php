<?php
namespace App\CoinExchange\Payments;

use Illuminate\Support\Facades\Config;

class PayPal {
    /**
     * @var \Netshell\Paypal\Paypal\Rest\ApiContext|null
     */
    private $apiContext = null;
    /**
     * @var \Netshell\Paypal\Paypal|null
     */
    private $paypal = null;
    /**
     * @var \Netshell\Paypal\Paypal\Api\Payment|null
     */
    private $payment = null;
    /**
     * PayPal constructor.
     * @param \Netshell\Paypal\Paypal $paypal
     * @param Config $config
     */
    public function __construct(\Netshell\Paypal\Paypal $paypal, Config $config) {
        $this->paypal = $paypal;
        $this->apiContext = $paypal->apiContext(
            $config->getFacadeRoot()->get('services.paypal.client_id'),
            $config->getFacadeRoot()->get('services.paypal.secret'));
        $this->apiContext->setConfig($config->getFacadeRoot()->get('paypal'));
        $this->payment = $this->paypal->payment();
    }

    /**
     * @param $transaction
     * @param $redirectUrls
     * @return mixed
     */
    public function depositFounds($transaction, $redirectUrls) {
        $this->payment->setIntent('sale');
        $this->payment->setPayer($this->createPayer());
        $this->payment->setRedirectUrls($redirectUrls);
        $this->payment->setTransactions([$transaction]);

        return $this->payment->create($this->apiContext);
    }

    public function setRedirectUrls($returnUrl, $cancelUrl) {
        $redirectUrls = $this->paypal->redirectUrls();
        $redirectUrls->setReturnUrl($returnUrl);
        $redirectUrls->setCancelUrl($cancelUrl);
        return $redirectUrls;
    }

    /**
     * @return \Netshell\Paypal\Paypal\Api\Payer
     */
    private function createPayer() {
        $payer = $this->paypal->payer();
        $payer->setPaymentMethod('paypal');
        return $payer;
    }

    /**
     * @param $deposit
     * @param string $currency
     * @return \Netshell\Paypal\Paypal\Api\Amount
     */
    public function setPaymentDetails($deposit, $currency="USD") {
        $amount = $this->paypal->amount();
        $amount->setCurrency($currency);
        $amount->setTotal($deposit);
        return $amount;
    }

    /**
     * @param $description
     * @param $amountObject
     * @param $userId
     * @return \Netshell\Paypal\Paypal\Api\Transaction
     */
    public function createTransaction($description, $amountObject, $userId) {
        $transaction = $this->paypal->transaction();
        $transaction->setAmount($amountObject);
        $transaction->setDescription($description);
        $transaction->setCustom($userId);
        return $transaction;
    }

    /**
     * @param $payment
     * @param $payerId
     * @return mixed
     */
    public function completePayment($payment, $payerId) {
        $paymentExecution = $this->paypal->paymentExecution();
        $paymentExecution->setPayerId($payerId);
        $paymentCompleted = $payment->execute($paymentExecution, $this->apiContext);
        return $paymentCompleted;

    }

    public function getPaymentById($paymentId) {
        return $this->paypal->getById($paymentId, $this->apiContext);
    }
}