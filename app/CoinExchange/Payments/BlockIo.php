<?php
namespace App\CoinExchange\Payments;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Config;

class BlockIo {
    /**
     * @var \BlockIo|null
     */
    private $blockIo = null;
    /**
     * @var string
     */
    private $parameter = 'label';
    /**
     * BlockIo constructor.
     * @param SessionManager $sessionManager
     * @param Config $config
     */
    public function __construct(SessionManager $sessionManager, Config $config) {

        $coin = is_null($sessionManager->get('coin')) ? 'btc' : $sessionManager->get('coin');

        $this->blockIo = new \BlockIo(
            $config->getFacadeRoot()->get("blockIo.{$coin}"),
            $config->getFacadeRoot()->get('blockIo.pin'));
    }

    /**
     * @param null $userId
     * @return null
     */
    public function createAddressForUser($userId = null) {
        $response = $this->blockIo->get_new_address([$this->parameter=>$userId]);

        if ($response->status !== 'success') return null;

        return $response->data->address;
    }

    /**
     * @param null $userId
     * @return mixed
     */
    public function getBalanceForUserId($userId = null) {
        return $this->blockIo->get_address_balance([$this->parameter=>$userId]);
    }

    /**
     * @param null $address
     * @return mixed
     */
    public function isGreenAddress($address = null) {
        return $this->blockIo->is_green_address(['addresses'=>$address]);
    }

    /**
     * @param null $userId
     * @return mixed
     */
    public function getAddressForUserId($userId = null) {
        $response = $this->blockIo->get_address_by_label([$this->parameter=>$userId]);

        if ($response->status !== 'success') return null;

        return $response->data->address;
    }

    public function getInfoForUserId($userId = null) {

        $response = $this->blockIo->get_address_by_label([$this->parameter=>$userId]);

        if ($response->status !== 'success') return null;

        return $response->data;
    }

    /**
     * @param int $amount
     * @param null $userId
     * @param null $address
     * @return mixed
     */
    public function withdrawToAddress($amount=0, $userId=null, $address=null) {

        return $this->blockIo->withdraw_from_label(['amounts'=>$amount,
            'from_labels'=>$userId, 'to_addresses'=>$address]);
    }

    /**
     * @param int $amount
     * @param null $fromUser
     * @param null $toUser
     * @return mixed
     */
    public function transferCoinsToLabel($amount=0, $fromUser=null, $toUser=null) {
        return $this->blockIo->withdraw_from_labels(['amounts'=>$amount,
            'from_labels'=>$fromUser, 'to_labels'=>$toUser]);
    }

    /**
     * @return float
     */
    public function getNetworkFee() {
        return 0.00003; // TODO needs to be in redis and updated from internet
    }
}