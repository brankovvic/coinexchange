<?php

namespace App\CoinExchange;

use App\CoinExchange\Payments\BlockIo;
use Illuminate\Redis\RedisManager;

class UserWallet
{
    /**
     * @var RedisManager|null
     */
    private $redis = null;
    /**
     * @var BlockIo|null
     */
    private $blockIo = null;

    /**
     * UserWallet constructor.
     * @param RedisManager $redisManager
     * @param BlockIo $blockIo
     */
    public function __construct(RedisManager $redisManager, BlockIo $blockIo)
    {
        $this->redis = $redisManager;
        $this->blockIo = $blockIo;
    }

    public function getCoinBalanceForUser($userId) {
        if ($this->redis->hexists('btcBalance', $userId)) {
            return $this->redis->hget('btcBalance', $userId);
        }
        $info = $this->blockIo->getInfoForUserId($userId);
        $amount = $info->available_balance;
        $this->redis->hset('btcBalance', $userId, $amount);
        $this->redis->expire('btcBalance', config('blockIo.btcBalanceExpire'));

        return $amount;
    }
}