<?php

namespace App\Listeners;

use App\Events\SomeEvent;
use App\CoinExchange\Payments\BlockIo;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateBlockIoAddresses
{
    /**
     * @var BlockIo|null
     */
    private $blockIo = null;

    /**
     * @var RedisManager|null
     */
    private $redis = null;

    /**
     * CreateBlockIoAddresses constructor.
     * @param RedisManager $redisManager
     * @param BlockIo $blockIo
     */
    public function __construct(RedisManager $redisManager, BlockIo $blockIo) {
        $this->blockIo = $blockIo;
        $this->redis = $redisManager;
    }

    /**
     * @param Registered $registered
     */
    public function handle(Registered $registered) {
        $address = $this->blockIo->createAddressForUser($registered->user->getAttribute('id'));

        if ( ! is_null($address)) {
            $this->redis->hset('btcAddress', $registered->user->getAttribute('id'), $address);
            $this->redis->hset('btcBalance', $registered->user->getAttribute('id'), 0.00);
            $this->redis->expire('btcBalance', config('blockIo.btcBalanceExpire'));
        }
    }
}