<?php

namespace App\Listeners;

use App\Events\SuccessfulPaymentEvent;
use App\User;
use App\UserPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class SuccessfulPayment
{
    /**
     * @var null|\Redis
     */
    private $redis = null;

    /**
     * SuccessfulPayment constructor.
     * @param User $user
     * @param Redis $redis
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis->getFacadeRoot();
    }

    /**
     * @param SuccessfulPaymentEvent $event
     */
    public function handle(SuccessfulPaymentEvent $event)
    {
        $event->user->update(['balance'=> $event->user->payments()->sum('amount')]);
    }
}
