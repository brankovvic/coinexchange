<?php

namespace App\Listeners;

use App\Events\SomeEvent;
use App\CoinExchange\Payments\BlockIo;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetBlockIoAddressToRedis
{
    /**
     * @var BlockIo|null
     */
    private $blockIo = null;
    /**
     * @var Request|null
     */
    private $request = null;
    /**
     * @var RedisManager|null
     */
    private $redis = null;

    /**
     * CreateBlockIoAddresses constructor.
     * @param Request $request
     * @param RedisManager $redisManager
     * @param BlockIo $blockIo
     */
    public function __construct(Request $request, RedisManager $redisManager, BlockIo $blockIo) {
        $this->blockIo = $blockIo;
        $this->request = $request;
        $this->redis = $redisManager;
    }

    /**
     *
     */
    public function handle() {

        if ($this->redis->hexists('btc', $this->request->user()->getAttribute('id')) === false) {
            $address = $this->blockIo->getInfoForUserId($this->request->user()->getAttribute('id'));

            $this->redis->hset('btc', $this->request->user()->getAttribute('id'), $address->address);

            $this->redis->hset('btcBalance', $this->request->user()->getAttribute('id'),
                $address->available_balance);
            $this->redis->expire('btcBalance', config('blockIo.btcBalanceExpire'));
        }
    }
}