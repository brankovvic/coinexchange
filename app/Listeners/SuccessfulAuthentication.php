<?php

namespace App\Listeners;

use App\CoinExchange\Payments\BlockIo;
use App\Events\SomeEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Redis\RedisManager;

class SuccessfulAuthentication
{
    /**
     * @var Request|null
     */
    private $request = null;
    /**
     * @var null|\Redis
     */
    private $redis = null;

    private $blockIo = null;
    /**
     * SuccessfulAuthentication constructor.
     * @param Request $request
     * @param \Redis $redis
     */
    public function __construct(Request $request, RedisManager $redisManager, BlockIo $blockIo)
    {
        $this->blockIo = $blockIo;
        $this->redis = $redisManager;
        $this->request = $request;
    }

    /**
     * @param \Redis $redis
     */
    public function handle()
    {
        try {
            $this->request->user()->authenticationHistory()->create([
                'agent'=>$this->request->header('User-Agent'),
                'ipAddress'=>$this->request->getClientIp(),
                'authDate'=>Carbon::now()
            ]);

            //set user authHistory in redis
           $this->redis->hSet('authHistory',
                $this->request->user()->getAttribute('id'),
                $this->request->user()->authenticationHistory()
                    ->orderBy('authDate','DESC')
                    ->limit(10)->get()->toJson());
            // put user id in session so we can cache the user object
            $this->request->session()->put('userId', $this->request->user()->getAttribute('id'));

            //set user object inside a redis
            $this->redis->hSet('users',
                $this->request->user()->getAttribute('id'),
                json_encode($this->request->user()));

        } catch(\Exception $e) {
            return redirect()->to('/login',500)
                ->withErrors('Internal Error Please Contact Site Admin');
        }
    }
}
