<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class LogoutHandler
{
    private $redisCoins = ['btc','litecoin','dogecoin'];
    /**
     * @var Request|null
     */
    private $request = null;
    /**
     * @var null|\Redis
     */
    private $redis = null;

    /**
     * SuccessfulAuthentication constructor.
     * @param Request $request
     * @param \Redis $redis
     */
    public function __construct(Request $request, Redis $redis)
    {
        $this->redis = $redis->getFacadeRoot();
        $this->request = $request;
    }

    /**
     * @param \Redis $redis
     */
    public function handle()
    {
        $this->redis->hDel('authHistory', $this->request->user()->getAttribute('id'));
        foreach ($this->redisCoins as $coin) {
            $this->redis->hDel($coin, $this->request->user()->getAttribute('id'));
        }
    }
}
