<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login'=> [
            'App\Listeners\SuccessfulAuthentication',
            'App\Listeners\SetBlockIoAddressToRedis'
        ],
        'Illuminate\Auth\Events\Registered' => [
            'App\Listeners\CreateBlockIoAddresses'
        ],

        'auth.register' => [
            'App\Listeners\SuccessfulAuthentication'

        ],

        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\LogoutHandler'
        ],
        'App\Events\SuccessfulPaymentEvent' => [
            'App\Listeners\SuccessfulPayment'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
