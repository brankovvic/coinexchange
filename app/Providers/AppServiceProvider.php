<?php

namespace App\Providers;

use App\UserLocation;
use GeoIp2\Model\City;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @param Request $request
     */
    public function boot(Request $request, Session $session, RedisManager $redisManager)
    {

        // everything from here needs to be placed in separate view composer
        if ($session->exists('coin') === false) {
            $session->put('coin','btc');
        }

        if ($request->hasCookie('userLocation') && $request->hasCookie('lat')
            && $request->hasCookie('lon')
        ) {
            $lat = $request->cookie('lat');
            $lon = $request->cookie('lon');
            View::share('userLocation', $request->cookie('userLocation'));
        } else {
            $userLocation = new UserLocation();
            $userLocationInfoInstance = $userLocation->readUserLocationInfoFromDatabase($request->getClientIp());
            if ($userLocationInfoInstance instanceof City) {
                $userLocationInfo = $userLocation->getUserLocationInfo($userLocationInfoInstance);
                $userLocation->setUserLocationToRequest($request, $userLocationInfo);
            } else {
                $userLocation->setUserLocationToRequest($request);
                $userLocationInfo = $userLocation->returnUserLocationInfo();
            }
            View::share('userLocation', $userLocationInfo['userLocation']);
            $lat = $userLocationInfo['lat'];
            $lon = $userLocationInfo['lon'];
        }

        if ( ! $session->exists('lat') &&  ! $session->exists('lon')) {
            $session->put('lat', $lat);
            $session->put('lon', $lon);
        }

        View::share('lat', $request->cookie('lat'));
        View::share('lon', $request->cookie('lon'));

        if ($session->exists('locationHash') !== true) {
            $session->put('locationHash', sha1($lat . $lon));
        }

        Schema::defaultStringLength(191);



        // it must boot application
        view()->composer('*', function($view) {
            $view->with('userAuthenticated', auth()->check());
        });

        view()->composer('layouts.includes.navigation', function($view) use ($redisManager) {
            if($redisManager->hexists('btcBalance', auth()->id()) === false) {
                $balance = round($redisManager->hget('btcBalance', auth()->id()),2);
            } else {
                $balance = 0.00;
            }
            $view->with('coinBalance', $balance);
        });

        view()->composer(
            'account.account', 'App\Http\ViewComposers\AccountViewComposer'
        );


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
