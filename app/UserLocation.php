<?php
namespace App;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

class UserLocation
{
    /**
     * @var Reader|null
     */
    private $reader = null;
    /**
     * @var array
     */
    private $userLocationInfo = [];

    /**
     * UserLocation constructor.
     */
    public function __construct()
    {
        $this->userLocationInfo = [
            'userLocation' => 'Belgrade, Serbia',
            'lat'=> '44',
            'lon'=> '20',
            'time'=>getenv('COOKIE_TIME')
        ];
        $this->reader = new Reader(base_path('database/GeoLite2-City.mmdb'));
    }

    /**
     * @param $userIp
     * @return \Exception|AddressNotFoundException|\GeoIp2\Model\City
     */
    public function readUserLocationInfoFromDatabase($userIp) {
        try {
            return $this->reader->city($userIp);
        } catch (AddressNotFoundException $addressNotFoundException) {
            return $addressNotFoundException;
        }
    }

    /**
     * @param $userLocationInfo
     * @return array
     */
    public function getUserLocationInfo($userLocationInfo) {
        if (array_key_exists(strtolower($userLocationInfo->country->isoCode),
            $userLocationInfo->country->names)) {
            if (!is_null($userLocationInfo->city->name)) {
                $this->userLocationInfo['userLocation'] =  $userLocationInfo->city->name . ', ' .
                    $userLocationInfo->country->names[strtolower($userLocationInfo->country->isoCode)];
            } else {
                $this->userLocationInfo['userLocation'] = $userLocationInfo->country->names[strtolower($userLocationInfo->country->isoCode)];
            }
        } else {
            // TODO check the correct name is isoCode not Found
            $this->userLocationInfo['userLocation'] = $userLocationInfo->country->names[strtolower($userLocationInfo->country->isoCode)];
        }

        $this->userLocationInfo['lat'] = $userLocationInfo->location->latitude;
        $this->userLocationInfo['lon'] = $userLocationInfo->location->longitude;
        $this->userLocationInfo['time'] = getenv('COOKIE_TIME');

        return $this->userLocationInfo;
    }

    /**
     * @param $request
     * @param array $userLocationInfo
     */
    public function setUserLocationToRequest(&$request, $userLocationInfo = array()) {
        if (!empty($userLocationInfo)) $this->userLocationInfo = $userLocationInfo;
        $request->merge($this->userLocationInfo);
    }

    /**
     * @param $response
     * @param array $userLocationInfo
     */
    public function setCookies(&$response, $userLocationInfo = array()) {
        if (!empty($userLocationInfo)) $this->userLocationInfo = $userLocationInfo;

        $response->withCookie(cookie('userLocation', $this->userLocationInfo['userLocation'], $this->userLocationInfo['time'] ));
        $response->withCookie(cookie('lat', $this->userLocationInfo['lat'], $this->userLocationInfo['time']));
        $response->withCookie(cookie('lon', $this->userLocationInfo['lon'], $this->userLocationInfo['time']));
    }

    /**
     * @return array
     */
    public function returnUserLocationInfo() {
        return $this->userLocationInfo;
    }
}