<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $fillable = [
        'userId',
        'trade_type',
        'currencyId',
        'price',
        'lat',
        'lon',
        'min_amount',
        'max_amount',
        'status',
        'authy_verified',
        'description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactTrades() {
        return $this->hasMany(TradeContract::class, 'adId', 'id');
    }

    /**
     * Returns A Search By Coordinates
     * @param $query
     * @param $lat
     * @param $lon
     * @return mixed
     */
    public function scopeSearchByCoordinates($query, $lat, $lon) {
        $lat = doubleval($lat);

        $lng = doubleval($lon);
        // we'll want everything within, say, 10km distance
        $distance = doubleval(1000) / 10000;

        // earth's radius in km = ~6371
        $radius = 6371;

        // latitude boundaries
        $maxlat = $lat + rad2deg($distance / $radius);
        $minlat = $lat - rad2deg($distance / $radius);

        // longitude boundaries (longitude gets smaller when latitude increases)
        $maxlng = $lng + rad2deg($distance / $radius / cos(deg2rad($lat)));
        $minlng = $lng - rad2deg($distance / $radius / cos(deg2rad($lat)));

        $query->whereBetween('lat',[$minlat,$maxlat]);
        $query->whereBetween('lon',[$minlng,$maxlng]);
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'userId');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSellAds($query) {
        $query->where('trade_type', config('trade.sell'));
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeBuyAds($query) {
        $query->where('trade_type', config('trade.buy'));
        return $query;
    }
    /**
     * @param $status
     * @return string
     */
    public function getStatusAttribute($status) {
        if ($status == 0) {
            return "Active";
        } else {
            return "Closed";
        }
    }
}
