<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    protected $fillable = [
      'userId','paymentOptionId','paymentId','amount'
    ];
}
