<?php

namespace App\Http\ViewComposers;

use App\Quote;
use Illuminate\Redis\RedisManager;
use Illuminate\View\View;

class AccountViewComposer
{
    /**
     * @var RedisManager|null
     */
    protected $redis = null;
    /**
     * @var array
     */
    protected $quote = null;

    /**
     * AccountViewComposer constructor.
     * @param RedisManager $redisManager
     * @param Quote $quote
     */
    public function __construct(RedisManager $redisManager, Quote $quote)
    {
        $this->quote = $quote;
        // Dependencies automatically resolved by service container...
        $this->redis = $redisManager;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {

        if ($this->redis->exists('quotes')) {
            $quotes = json_decode($this->redis->get('quotes'), true);
            $quote = $quotes[rand(0, count($quotes) - 1)];
        } else {
            $quotes = $this->quote->get()->toArray();
            $quote = $quotes[rand(0, count($quotes) - 1)];
            $this->redis->set('quotes', json_encode($quotes));
        }
        $view->with('quote', $quote);
    }
}