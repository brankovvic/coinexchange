<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lat' => 'required',
            'lon' => 'required',
            'currencyId' => 'required|numeric|exists:countries,id',
            'min_amount' => 'required|numeric',
            "max_amount'=>'required|numeric|min:{$this->get('min_amount')}|max:100000",
            'description' => 'required|min:5|max:200',
            "trade_type'=>'required|in:".implode([config('trade.buy'), config('trade.sell')]),
            'authy_verified' => 'in:0,1'
        ];
    }
}
