<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|max:30|min:3|alpha',
            'lastName' => 'required|max:30|min:3|alpha',
            'username'=>'required|max:30|min:3|alpha',
            'password'=>'min:8|max:40|confirmed|nullable'
        ];
    }
}
