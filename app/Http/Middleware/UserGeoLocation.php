<?php

namespace App\Http\Middleware;

use App\UserLocation;
use Closure;
use GeoIp2\Model\City;
use Illuminate\Http\Response;
use Psy\Exception\FatalErrorException;

class UserGeoLocation
{
    private $userLocation = null;

    public function __construct(){
        $this->userLocation = new UserLocation();
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws FatalErrorException
     */
    public function handle($request, Closure $next) {
        if ($request->hasCookie('userLocation') && $request->hasCookie('lat') &&
            $request->hasCookie('lon')
        ) {
            return $next($request);
        }
        if ($request->has('userLocation') && $request->has('lat') &&
            $request->has('lon')
        ) {
            $response = $next($request);

            $this->userLocation->setCookies($response, $request->toArray());

            return $response;

        } else {
            return $next($request);
        }
    }
}
