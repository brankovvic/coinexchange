<?php

namespace App\Http\Middleware;

use Closure;

class TwoFactorEnable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! is_null($request->user()->getAttribute('authId')) &&
            $request->user()->getAttribute('authOn') == true) {
            return $next($request);
        }

        return redirect()->route('showTwoFactorHelp')->withErrors('Please Enable Two Factor');
    }

}
