<?php

namespace App\Http\Controllers;

use App\Http\Requests\EnableTwoFactorAuthentication;
use Authy\AuthyApi;
use Illuminate\Http\Request;

class TwoFactorController extends Controller
{
    /**
     * @var AuthyApi|null
     */
    private $twoFactor = null;

    /**
     * TwoFactorController constructor.
     */
    public function __construct() {

        $this->twoFactor = new AuthyApi(getenv('AUTHY_KEY'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showHelp(Request $request) {
        return view('twoFactor.twoFactor', ['user' => $request->user()]);
    }

    /**
     * @param EnableTwoFactorAuthentication $authentication
     * @return $this
     */
    public function enableTwoFactor(EnableTwoFactorAuthentication $authentication) {

        if (!is_null($authentication->user()->getAttribute('authId'))) {
            return redirect()->back()->withErrors('Two Factor Already Enable on Your Account');
        }

        // create account for user
        $user = $this->twoFactor->registerUser($authentication->user()->getAttribute('email'),
            $authentication->get('mobile'), $authentication->get('countryCode'));

        if ($user->ok() === false) {
            dd($user->errors());
            return redirect()->back()->withErrors($user->errors());
        }

        if ($authentication->user()->update([
                'mobile' => $authentication->get('mobile'),
                'countryCode' => $authentication->get('countryCode'),
                'authId' => $user->id(),
                'authOn' => 1
            ]) === false
        ) {
            return redirect()->back()->withErrors('Internal Error Please Contact Support');
        }

        $user = $this->twoFactor->requestSms($user->id());

        if ($user->ok() === false) {
            return redirect()->back()->withErrors($user->errors());
        }

        return redirect()->route('showConfirmation')->with('message', 'SMS Successfully Sent');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showConfirmation() {
        return view('twoFactor.twoFactorVerification');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function checkConfirmation(Request $request) {
        $user = $this->twoFactor->verifyToken($request->user()->getAttribute('authId'),
            $request->get('token'));
        if ($user->ok()) {
            return redirect()->route('account')->with('message','Two Factor Successfully Enabled');
        }

        return redirect()->back()->withErrors('Please Verify If Your Token Is Correct');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function requestCall(Request $request) {
        $user = $this->twoFactor->phoneCall($request->user()->getAttribute('authId'));
        if ($user->ok()) {
            return redirect()->back('200')->with('message','Call in Progress');
        }
        return redirect()->back('500')->with($user->errors());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resendSms(Request $request) {
        $user = $this->twoFactor->requestSms($request->user()->getAttribute('authId'));
        if ($user->ok()) {
            return redirect()->back()->with('message','SMS Sent');
        }
        return redirect()->back()->with($user->errors());
    }
}
