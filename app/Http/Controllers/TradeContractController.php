<?php

namespace App\Http\Controllers;

use App\CoinExchange\Payments\BlockIo;
use App\CoinExchange\UserWallet;
use App\TradeContract;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\DB;

class TradeContractController extends Controller
{
    /**
     * @var UserWallet|null
     */
    private $userWallet = null;

    /**
     * TradeContractController constructor.
     * @param UserWallet $userWallet
     */
    public function __construct(UserWallet $userWallet) {
        $this->userWallet = $userWallet;
    }

    /**
     * @param Request $request
     * @param TradeContract $tradeContract
     * @param BlockIo $blockIo
     * @param RedisManager $redisManager
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function approveNewContract(Request $request, TradeContract $tradeContract, BlockIo $blockIo, RedisManager $redisManager) {
        // check if request is correct
        if ( ! $request->has('contractId')) return redirect()->back()->withErrors('Bad Request');

        $contract = $tradeContract->where('id', $request->get('contractId'))->first();

        // check if contract exists
        if (is_null($contract)) return redirect()->back()->withErrors('Bad Request');

        // check if user have coin balance
        $coinBalance = $this->userWallet->getCoinBalanceForUser(auth()->id());

        $needCoins = (double)($contract->getAttribute('coinPrice') + $blockIo->getNetworkFee());

        if ($needCoins > (double)$coinBalance) {
            return redirect()->back()->withErrors('You Cannot Approve Contract! Not Enough Coins');
        }

        try {
            DB::beginTransaction();

            // transfer money to user account
            auth()->user()->update([
                'balance'=> auth()->user()->getAttribute('balance') + $contract->getAttribute('price')
            ]);

            // transfer coins to user wallet
            $blockIo->transferCoinsToLabel($contract->getAttribute('coinPrice'),
                auth()->id(), $contract->getAttribute('userId'));

            $contract->update(['status'=>1]); // TODO Needs To Be In config

            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return redirect()->back()->withErrors('Internal Error Please Try Again');
        }

        $redisManager->publish('tradeNotification', json_encode([
                'tradeId' => $contract->getAttribute('id'),
                'type'=>1, // TODO Needs To Be In config
                'message'=>"Your Trade Request Has Been Approved",
                'notifyUserId'=> $contract->getAttribute('userId')
            ]
        ));

        return redirect()->back()->with(['''Message Contract Approved']);

    }
}
