<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Http\Requests\CreateAdRequest;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Webpatser\Countries\Countries;

class AdsController extends Controller
{
    /**
     * @param RedisManager $redisManager
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreate(RedisManager $redisManager) {
        if ($redisManager->exists('countries')) {
            $countries = json_decode($redisManager->get('countries'));
        } else {
            $countries = Countries::all(['id','currency_code', 'iso_3166_2']);
            $redisManager->set('countries', json_encode($countries));
        }
        return view('ads.index', ['countries'=>$countries]);
    }

    /**
     * @param CreateAdRequest $createTradeRequest
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function createTrade(CreateAdRequest $createTradeRequest) {
        try {
            $createTradeRequest->user()->ads()->create($createTradeRequest->all());
            return redirect()->route('showDashboard')->with('message', 'Trade Created');
        } catch(\Exception $e) {
            return redirect()->back()->withErrors('Failed To Create Your Trade Please Try Again');
        }
    }

    /**
     * @param Ads $ads
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showBuyAds(Ads $ads) {
        $buyAds = $ads->searchByCoordinates(session('lat'), session('lon'))
            ->buyAds()->with('user')->where('userId', '!=', auth()->user()->getAttribute('id'))
            ->limit(5)->get(); // user with or the relation uses select every time
        return view('ads.buy', ['ads'=>$buyAds]);
    }

    /**
     * @param Ads $ads
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSellAds(Ads $ads) {
        $sellAds = $ads->searchByCoordinates(session('lat'), session('lon'))
            ->sellAds()->with('user')->where('userId', '!=', auth()->user()->getAttribute('id'))
            ->limit(5)->get(); // user with or the relation uses select every time
        return view('ads.sell', ['ads'=>$sellAds]);
    }

    public function showUserTrades() {
        dd("show user trades");
    }
}
