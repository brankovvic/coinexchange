<?php

namespace App\Http\Controllers;

use App\Events\SuccessfulPaymentEvent;
use App\Http\Requests\UpdateAccountProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class AccountController extends Controller
{
    /**
     * @var mixed|null
     */
    private $redis = null;

    /**
     * AccountController constructor.
     * @param Redis $redis
     */
    public function __construct(Redis $redis) {
        $this->redis = $redis->getFacadeRoot();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function account(Request $request) {

        if ($this->redis->hexists('authHistory', $request->user()->getAttribute('id'))) {
            $authHistory = json_decode($this->redis->hGet('authHistory', $request->user()->getAttribute('id')));
        } else {
            $authHistory = $request->user()->authenticationHistory()->limit(10)
                ->orderBy('authDate','DESC')->get();
        }

        return view('account.account', [
            'authHistory'=>$authHistory,
            'user'=>$request->user()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request) {
        return view('account.edit',['user'=>$request->user()]);
    }

    /**
     * @param UpdateAccountProfile $accountProfile
     * @param User $user
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(UpdateAccountProfile $accountProfile, User $user) {
        try {
            // must do manuel checks do to laravel validation restrictions
            if ($accountProfile->get('username') !== $accountProfile->user()->getAttribute('username')) {
                if ($user->where('username', $accountProfile->get('username'))->count() > 0) {
                    return redirect()->back()->withErrors(['username'=>'Username Must Be Unique']);
                }
            }
            if(is_null($accountProfile->get('password'))) {
                $accountProfile->user()->update($accountProfile->except('password'));
            } else {
                $accountProfile->user()->update($accountProfile->all());
            }
            return redirect()->back()->with('message','Account Successfully Updated');
        } catch(\Exception $e) {
            return redirect()->back()->withErrors('Internal Error Failed To Update Profile');
        }
    }
}
