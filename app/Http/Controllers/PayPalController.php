<?php

namespace App\Http\Controllers;

use App\AvailablePayments;
use App\CoinExchange\Payments\PayPal;
use App\Events\SuccessfulPaymentEvent;
use App\Http\Requests\PaymentRequest;
use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;

class PayPalController extends Controller
{
    private $payPal = null;

    /**
     * PayPalController constructor.
     * @param PayPal $payPal
     */
    public function __construct(PayPal $payPal) {
        $this->payPal = $payPal;
    }

    public function showPayment() {
        return view('testingViews.testPayment');
    }

    /**
     * @param PaymentRequest $paymentRequest
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function makeDeposit(PaymentRequest $paymentRequest) {
        $amount = $this->payPal->setPaymentDetails($paymentRequest->get('amount'));

        if ( ! ($amount instanceof Amount)) {
            return redirect()->back()->withErrors(['error','Internal Error Please Try Again']);
        }
        $urls = $this->payPal->setRedirectUrls(route('payPalCompleted'), route('payPalCompleted'));

        if (!($urls instanceof RedirectUrls)) {
            return redirect()->back()->withErrors(['error','Internal Error Please Try Again']);
        }

        $transaction = $this->payPal->createTransaction("Deposit Founds", $amount,
            $paymentRequest->user()->getAttribute('id'));

        if (!($transaction instanceof Transaction)) {
            return redirect()->back()->withErrors(['error','Internal Error Please Try Again']);
        }

        try {
            $response = $this->payPal->depositFounds($transaction, $urls);
        } catch(PayPalConnectionException $e) {
            dd($e->getMessage());
            return redirect()->back()->withErrors(['error','PayPal Service Error']);
        } catch(\Exception $e) {
            return redirect()->back()->withErrors(['error','Internal Error Please Try Again']);
        }

        return redirect()->to($response->links[1]->href);
    }

    public function paymentComplete(Request $request, AvailablePayments $availablePayments) {
        $payment = $this->payPal->getPaymentById($request->get('paymentId'));

        if (empty($payment->transactions)) {
            return redirect()->route('account')->withErrors('Failed To Complete The Payment');
        }

        $response = $this->payPal->completePayment($payment,
            $request->get('PayerID'));

        if ($response->state !== 'approved') {
            return redirect()->route('account')->withErrors('Your Payment Is Not Approved');
        }

        try {
            $request->user()->payments()->create([
                'userId'=>$response->transactions[0]->custom,
                'paymentOptionId'=>$availablePayments->where('payment_name','paypal')->first()->getAttribute('id'),
                'paymentId'=>$request->get('paymentId'),
                'amount'=>$response->transactions[0]->amount->total
            ]);
            event(new SuccessfulPaymentEvent($request->user()));
            return redirect()->route('account')->with('message','Payment Is Successful');
        } catch(\Exception $e) {
            return redirect()->route('account')->withErrors('Payment is Completed But Failed to be Saved in our Database...
            Contact Support and pass them payment id ' . $request->get('paymentId'));
        }
    }

    public function paymentCancel(Request $request) {
        dd('Payment Cancel');
    }
}
