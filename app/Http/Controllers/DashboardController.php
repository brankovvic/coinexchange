<?php

namespace App\Http\Controllers;

use App\Ads;
use App\TradeContract;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @param TradeContract $tradeContract
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(TradeContract $tradeContract) {

        $userId = auth()->user()->id;

        $tradeContracts = $tradeContract->whereIn('adId', function($query) use ($userId) {
            $query->select('id')->from('ads')->where('userId', $userId)->where('status', 0);
        })->where('status', 0)->paginate(5); //TODO needs be in config

        return view('dashboard.index', [
            'ads'=>auth()->user()->ads()->paginate(5),
            'tradesContracts'=> $tradeContracts,
            'tradeContractRequests'=>auth()->user()->contactTrades()->with('ad.user')->paginate(5)
        ]);
    }
}
