<?php

namespace App\Http\Controllers;

use App\Ads;
use App\CoinExchange\Payments\BlockIo;
use App\Escrow;
use App\Http\Requests\CreateAdRequest;
use App\Http\Requests\CreateTradeRequest;
use App\TradeContract;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\DB;

class TradeController extends Controller
{
    /**
     * @var Ads|null
     */
    private $ads = null;

    /**
     * TradeController constructor.
     * @param Ads $ads
     */
    public function __construct(Ads $ads)
    {
        $this->ads = $ads;
    }

    public function showCreateTrade($adId = null)
    {
        $ad = $this->ads->where('id', $adId)->with('user')->first();

        if (is_null($ad)) return redirect()->back()->withErrors('Trade Not Found');

        return view('trades.index', ['ad' => $ad]);
    }

    /**
     * @param $adId
     * @param CreateTradeRequest $tradeRequest
     * @param Escrow $escrow
     * @param TradeContract $tradeContract
     * @param RedisManager $redisManager
     * @return $this
     */
    public function createNewTrade($adId, CreateTradeRequest $tradeRequest, Escrow $escrow,
                                   TradeContract $tradeContract, RedisManager $redisManager)
    {

        // TODO Check if the current has more then N number of trades with same user and block
        $ad = $this->ads->where('id', $adId)->with('user')->first();

        if (is_null($ad)) return redirect()->back()->withErrors('Trade Not Found');

        if (auth()->user()->getAttribute('balance') < $tradeRequest->get('price')) {
            $amountToDeposit = $tradeRequest->get('price') - auth()->user()->getAttribute('balance');
            return redirect()->route('depositFounds')->withInput(['amount' => (int)$amountToDeposit]);
        }

        try {

            DB::beginTransaction();

            $trade = $tradeContract->create([
                'adId' => $ad->getAttribute('id'),
                'userId' => auth()->user()->getAttribute('id'),
                'price' => $tradeRequest->get('price'),
                'coinPrice' => $tradeRequest->get('coinPrice')
            ]);

            $escrow->create([
                'tradeContractId' => $trade->getAttribute('id'),
                'price' => $tradeRequest->get('price'),
            ]);

            auth()->user()->update(['balance' => auth()->user()->getAttribute('balance') - $tradeRequest->get('price')]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());

        }
        $username = ucfirst(auth()->user()->getAttribute('username'));

        $redisManager->publish('tradeNotification', json_encode([
                'tradeId' => $trade->getAttribute('id'),
                'type'=>0, // TODO Needs To Be In config
                'message'=>"You Have New Trade Request From User {$username}",
                'notifyUserId'=> $ad->getAttribute('userId')
            ]
        ));

        return redirect()->route('showDashboard')
            ->with(['message' => 'Trade Successfully Created! Notification Request Sent To User.']);
    }
}
