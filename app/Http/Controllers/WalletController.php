<?php

namespace App\Http\Controllers;

use App\CoinExchange\Payments\BlockIo;
use App\CoinExchange\UserWallet;
use App\Http\Requests\TransferCoinAmountRequest;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;

class WalletController extends Controller
{
    /**
     * @var RedisManager|null
     */
    private $redis = null;
    /**
     * @var BlockIo|null
     */
    private $blockIo = null;
    /**
     * WalletController constructor.
     * @param RedisManager $redisManager
     */
    public function __construct(RedisManager $redisManager, BlockIo $blockIo)
    {
        $this->redis = $redisManager;
        $this->blockIo = $blockIo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(UserWallet $userWallet) {
        if ($this->redis->hexists('btcAddress', auth()->id())) {
            $address = $this->redis->hget('btcAddress', auth()->id());
        } else {
            $info = $this->blockIo->getInfoForUserId(auth()->id());
            $address = $info->address;
        }

        $amount = $userWallet->getCoinBalanceForUser(auth()->id());

        /*if ($this->redis->hexists('coinFee','btc')) {
            $networkFee = $this->redis->hget('coinFee','btc');
        } else {

        }*/
        $fee = 0.0003; // TODO Needs modal for confirmation regarding fee
        return view('wallet.index',['address'=>$address, 'amount'=>$amount, 'fee'=>$fee]);
    }

    public function transferCoinAmount(TransferCoinAmountRequest $amountRequest) {
        try {
            $this->blockIo->withdrawToAddress($amountRequest->get('coinAmount'),
            auth()->user()->getAttribute('id'), $amountRequest->get('address'));
            return redirect()->back()->with(['message'=>'Coins Sent']);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
