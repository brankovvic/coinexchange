<?php

namespace App\Http\Controllers;

use App\Ads;

class IndexController extends Controller
{
    /**
     * @param Ads $ads
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Ads $ads) {
        $buyAds = $ads->searchByCoordinates(session('lat'), session('lon'))
            ->buyAds();

        if(auth()->check()) {
            $buyAds->where('userId', '!=', auth()->user()->getAttribute('id'));
        }

        $buyAds = $buyAds->with('user')->limit(5)->get();

        return view('home', ['buyAds'=>$buyAds]);
    }
}
