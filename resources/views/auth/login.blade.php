@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Login')

{{-- Inline styles --}}
@section('styles')
    <link href="{{ URL::to('assets/css/auth.css') }}" rel="stylesheet">
    <link href="{{ URL::to('assets/css/bootstrap.social.css') }}" rel="stylesheet">
@stop

{{-- Page content --}}
@section('page')
    <section class="auth">
        <div class="col-sm-8 col-md-8 col-md-offset-2">
            <div class="auth__wall row">
                <div class="col-md-6">

                    <div class="auth__profile">
                        <img src="{{ asset('assets/img/brand-sentinel.svg') }}" alt="Profile Image">
                    </div>
                    <!-- Social logins -->
                    <div class="auth__social">
                        <h3>Sign in or register!</h3>

                        <p>Fast and Simple authentication through OAuth 1 &amp; 2 service providers.</p>
                        <a href="#" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook fa-lg"></i></a>
                        <hr class="visible-sm">
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified nav-auth" role="tablist">
                        <li class="active"><a href="#login" role="tab" data-toggle="tab"><i class="fa fa-user"></i>
                                Login</a></li>
                        <li><a href="#register" role="tab" data-toggle="tab"><i class="fa fa-user-plus"></i>
                                Register</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane active" id="login">
                            @include('auth.includes.login')
                        </div>

                        <div class="tab-pane" id="register">
                            @include('auth.includes.register')
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit" defer></script>
    <script>
        var recaptcha1;
        var recaptcha2;
        var myCallBack = function () {

            recaptcha1 = grecaptcha.render('recaptcha1', {
                'sitekey': '#',
                'theme': 'light'
            });


            recaptcha2 = grecaptcha.render('recaptcha2', {
                'sitekey': '#',
                'theme': 'light'
            });
        };
    </script>

@stop
