<form action="{{url('/register')}}" method="post" class="auth__form form-horizontal validate-form"
      data-bv-message="This value is not valid"
      data-bv-feedbackicons-valid="fa fa-check-sqaure-o fa-lg"
      data-bv-feedbackicons-invalid="fa fa-warning"
      data-bv-feedbackicons-validating="fa fa-refresh">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group @if ($errors->has('username')) has-error @elseif(old('username')) has-success @endif">
        <input class="form-control" value="{{old('username')}}" name="username" type="text" placeholder="User Name" required autofocus />
        @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group @if ($errors->has('firstName')) has-error @elseif(old('firstName')) has-success @endif">
        <input class="form-control" name="firstName" value="{{old('firstName')}}" type="text" placeholder="First Name" required autofocus />
        @if ($errors->has('firstName'))
            <span class="help-block">
                <strong>{{ $errors->first('firstName') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group @if ($errors->has('lastName')) has-error @elseif(old('lastName')) has-success @endif">
        <input class="form-control" name="lastName" value="{{old('lastName')}}" type="text" placeholder="Last Name" required autofocus />
        @if ($errors->has('lastName'))
            <span class="help-block">
                <strong>{{ $errors->first('lastName') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group  @if ($errors->has('email')) has-error @elseif(old('email')) has-success @endif">
        <input class="form-control" name="email" value="{{old('email')}}" type="email" placeholder="Email" required autofocus data-bv-emailaddress-message="The input is not a valid email address" />
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group @if ($errors->has('password')) has-error @endif">
        <input class="form-control" name="password" type="password" placeholder="Password" required
               data-bv-notempty="true"
               data-bv-notempty-message="The password is required and cannot be empty" />
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group @if ($errors->has('password')) has-error @endif">
        <input class="form-control" name="password_confirmation" type="password" placeholder="Confirm Password" required
               data-bv-notempty="true"
               data-bv-notempty-message="The password is required and cannot be empty"
               data-bv-identical="true"
               data-bv-identical-field="password"
               data-bv-identical-message="The password and its confirm are not the same" />
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <div id="recaptcha2"></div>
    </div>
    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
    </div>

</form>
