@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Chat')

@section('styles')
    <link href="{{ asset('chat/chat.css') }}" rel="stylesheet">
@stop
@section('content')
    <div id="page">

    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="chat/bundle.js"></script>
@stop