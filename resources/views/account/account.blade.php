@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Account Profile')

{{-- Inline styles --}}
@section('styles')
    <link href="{{ asset('assets/css/account.css') }}" rel="stylesheet">
    <style>
        .dot {
            display: inline-block;
            width: 185px;
            white-space: nowrap;
            overflow: hidden !important;
            text-overflow: ellipsis;
        }
    </style>
@stop

{{-- Page content --}}
@section('page')
    <section class="account">

        <header class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="{{route('showEditAccount')}}">Edit Profile</a>
            </div>
        </header>

        <div class="col-sm-6 col-md-6 col-md-offset-3">
            <!-- Account -->
            <div class="row text-center">
                <div class="account__profile-image">
                    <img src="{{ asset('assets/img/brand-sentinel.svg') }}" alt="Profile Image">
                </div>
                <p class="account__roles">
                    <span class="label label-default">User</span>
                </p>
                <h3>{{$user->fullName}}<br>
                    <small>{{ $user->getAttribute('email') }}</small>
                </h3>
                <p>{{$quote['quote']}} ~
                    <small>{{$quote['author']}}</small>
                </p>
                <hr>
            </div>
            <div class="row">
                <!-- Active User Sessions -->
                <div class="panel panel-default">

                    <div class="panel-heading clearfix">
                        <div class="pull-left">Sessions History</div>
                        <div class="pull-right">
                            <a href="#" class="btn btn-sm btn-default">Flush</a>
                            <a href="#" class="btn btn-sm btn-default">Flush All</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach ($authHistory as $index => $item)
                                @if($index ==0)
                                    <a href="#" class="list-group-item active">
                                        {{ \Carbon\Carbon::parse($item->authDate)->format('F d, Y - h:ia') }}
                                        <span class="badge">{{ $item->ipAddress }}</span>
                                        <span class="badge dot">{{ $item->agent }}</span>
                                        <span class="badge">You</span>
                                        <span class="label label-default"><a
                                                    href='#'>remove</a></span>
                                    </a>
                                @else
                                    <a class="list-group-item">
                                        {{ \Carbon\Carbon::parse($item->authDate)->format('F d, Y - h:ia') }}
                                        <span class="badge">{{ $item->ipAddress }}</span>
                                        <span class="badge dot">{{ $item->agent }}</span>
                                        <span class="label label-default"><a
                                                    href='#'>remove</a></span>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

