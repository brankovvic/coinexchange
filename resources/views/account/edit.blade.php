@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Update Profile')

{{-- Page content --}}
@section('page')
    <section class="user">
        <header class="page-header">
            <h1>Update your profile</h1>
        </header>
        <div class="col-md-1 col-md-offset-1">
            <div class="auth__profile">
                <img src="{{asset('assets/img/brand-sentinel.svg')}}" alt="Profile Image">
            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-md-offset-2">
            <form method="post" action="{{route('updateAccount')}}" autocomplete="off" class="validate-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="patch">
                <!-- Hack to completely disable autocompletion -->
                <input type="text" style="display: none;">
                <input type="password" style="display: none;">

                <div class="form-group {{ $errors->first('firstName', ' has-error') }}">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" name="firstName" id="firstName"
                           value="{{ old('firstName', $user->getAttribute('firstName')) }}"
                           placeholder="Enter your first name.">
                    <span class="help-block">
                        {{ $errors->first('firstName') }}
                    </span>
                </div>

                <div class="form-group{{ $errors->first('lastName', ' has-error') }}">
                    <label for="name">Last Name</label>
                    <input type="text" class="form-control" name="lastName" id="lastName"
                           value="{{ old('lastName', $user->getAttribute('lastName')) }}"
                           placeholder="Enter your last name.">
                    <span class="help-block">{{ $errors->first('lastName') }}</span>
                </div>
                <div class="form-group{{ $errors->first('username', ' has-error') }}">
                    <label for="name">Username</label>
                    <input type="text" class="form-control" name="username" id="username"
                           value="{{ old('username', $user->getAttribute('username')) }}"
                           placeholder="Enter your desired username">
                    <span class="help-block">{{ $errors->first('username') }}</span>
                </div>
                <div class="form-group{{ $errors->first('password', ' has-error') }}">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password"
                           placeholder="Enter the user password (only if you want to modify it)."
                           data-bv-identical="true"/>
                    <span class="help-block">{{ $errors->first('password') }}</span>
                </div>
                <div class="form-group{{ $errors->first('password_confirmation', ' has-error') }}">
                    <label for="password_confirm">Confirm Password</label>
                    <input class="form-control" name="password_confirmation" type="password"
                           placeholder="Confirm Password"
                           data-bv-identical="true"
                           data-bv-identical-field="password"
                           data-bv-identical-message="The password and its confirmation are not the same!"
                    />
                    <span class="help-block">
                        {{ $errors->first('password_confirmation') }}</span>
                </div>
                <hr>
                <div class="text-right">
                    <a class="btn btn-default" href="#">Cancel</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </section>
@stop