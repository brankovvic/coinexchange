@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Two Factor')

@section('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/authy-form-helpers/2.3/form.authy.min.css" media="screen"
          rel="stylesheet" type="text/css">
@stop
{{-- Page content --}}
@section('page')

    <h3 class="v-margin20">Secure your account with two-factor authentication</h3>

    @if(! is_null($user->getAttribute('authId')))
        <div class="alert alert-success">
            2 Factor is enabled on your account.
        </div>
    @else
        <div class="alert alert-warning">
            2 Factor is not enabled on your account.
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="well v-margin20">
                <h4>What is two-factor authentication?</h4>

                <img class="2fa-snippet-img" alt="" src="{{asset('images/twofactor.png')}}">

                <ul>
                    <li>To login, you need one-time code from mobile phone or printed paper in addition to your
                        password.
                    </li>
                    <li>Two-factor authentication works with <strong>mobile apps</strong> (Android, iOS, Windows
                        Mobile).
                    </li>
                    <li>We use Authy as our Two Factor provider. Which allows Mobile and Desktop app for Two-step
                        Verification
                    </li>
                </ul>
                <h4>Why two-factor authentication?</h4>
                <ul>
                    <li><strong>More than 99%</strong> of the attacks against you can be prevented with two-factor
                        authentication
                    </li>
                    <li>Two-factor authentication codes are very hard to steal, unlike passwords.</li>
                    <li><strong>It takes only few minutes to set it up!</strong></li>
                </ul>
                <h4>How to use ?</h4>
                <ul>
                    <li>Authy makes it really easy to use Two-Factor Authentication on your online accounts using your
                        smartphone.
                    </li>
                    <li>They provide you an App that makes it easy for you to keep all your tokens and "just" works for
                        a strong authentication..
                    </li>
                    <li>Once you Enable TwoFactor on your account go straight to : https://www.authy.com/personal/ to
                        download the app
                    </li>
                </ul>
                <h4>What if i don't have a smartphone ?</h4>

                <p>
                    In such a case you would automatically recive an SMS on you're phone.
                </p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="well v-margin20">
                <form action="{{route('enableTwoFactor')}}" method="post" autocomplete="off">
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="checkbox">
                            <label>
                                <input type="checkbox"
                                       name="authyId" {{ (!is_null($user->getAttribute('authId'))) ? 'checked' : null }}>Two
                                Factor Authentication</label>
                            <small>Enable / Disable 2 Factor Authentication for your account.</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <input style="display:none" id="authy-countries" class="form-control" name="countryCode" size="30"
                               value="{{ (!is_null($user->getAttribute('countryCode'))) ? 'checked' : null }}"
                               autocomplete="off" type="text"/>
                    </div>
                    <div class="form-group">
                        <label>Cellphone</label>
                        <input style="width:15%" id="authy-cellphone" type="number" class="form-control"
                               value="{{ (!is_null($user->mobile)) ? $user->mobile : null }}" autocomplete="off"
                               name="mobile" placeholder="12345678"/><br/>
                        <br/>
                        <span>{{$errors->first('mobile')}}</span>
                    </div>
                    <input type="submit"
                           class="btn {{ (!is_null($user->getAttribute('authId'))) ? 'btn-danger' : 'btn-primary' }}"
                           value="Update Account">

                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <dl>
                <dt>
                <h3>Other security measures</h3></dt>
                <dd>
                    <p><strong>Do not use the same password on different websites.</strong></p>

                    <p>Do not use <strong>Tor browser</strong>. <a
                                href="http://lifehacker.com/how-can-i-stay-anonymous-with-tor-1498876762">Using a Tor
                            browser puts you in the risk getting your bitcoins stolen.</a></p>

                    <p>Do not get involved in transactions outside the website.</p>

                    <p>Do not use the website from a <strong>shared computers or devices, like ones in public internet
                            cafes</strong>, as they may have keyloggers installed to steal your user credentials.</p>

                    <p>When logging in to the website, <strong>read the browser address bar</strong>.
                    </p>
                    <!-- <p class="text-center">
                         <img alt="" src="/cached-static/img/guides/addressbar.png">
                     </p>-->
                    <p>Do not click Google advertisements.</p>

                    <p>If possible, when accessing Bitcoin wallets, do this from a dedicated computer you have reserved
                        for financial tasks only. Do not use this computer for other tasks.</p>

                    <p>Do not install third party software, pirated software or browser addons you cannot trust 100%.
                        This greatly reduces the risk of getting Bitcoin stealing malware infection on the computer.</p>
                </dd>
            </dl>
        </div>
    </div>
@stop

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/authy-form-helpers/2.3/form.authy.min.js"
            type="text/javascript"></script>
    <script>
        $(document).ready(function() {
           $('#countries-input-0').className = "from-control";
        });
    </script>
@stop