@extends('layouts.master')

{{-- Page title --}}
@section('title', '2-Step Verification')

{{-- Page content --}}
@section('page')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-mobile"></i> 2-Step Verification</div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="{{route('checkConfirmation')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Enter Token</label>
                                <div class="col-md-6">
                                    <input id="authToken" type="string" class="form-control" name="token" value="">
                                    <br>
                                    <p>Open the two-factor authentication app on your device to view your authentication code and verify your identity.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary" style="margin-right: 15px;">Verify</button>
                                    <a href="{{ route('resendSms') }}" class="btn btn-primary">Resend SMS</a>
                                    <a href="{{ route('requestCall') }}" class="btn btn-primary">Request Call</a>
                                    <p>
                                        &nbsp;
                                    </p>
                                    <a class="btn btn-primary">Re-enter Number</a>
                                    <a class="btn btn-primary">Enable it Later</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop