<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand brand" href="{{ route('index') }}">BTC Exchange</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li {{ request()->is('buy') ? ' class="active"' : null }}><a href="{{route('buy')}}"> <i class="fa fa-btc"></i> Buy
                        Bitcoins</a></li>
                <li {{ request()->is('sell') ? ' class="active"' : null }}><a href="{{route('sell')}}"> <i class="fa fa-money"></i> Sell
                        Bitcoins</a></li>
                <li {{ \Illuminate\Support\Facades\Request::is('create') ? ' class="active"' : null }}>
                    <a href="{{route('showCreate')}}"> <i class="fa fa-exchange"></i>
                        Trade</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li id="currentLocation"><a>
                        <i class="fa"></i>
                        Current Location : {{$userLocation}}</a>
                </li>
                <li>
                    <a data-target="#us6-dialog" data-toggle="modal"><i class="fa fa-globe"></i> Select Location</a>
                </li>
                @if( ! $userAuthenticated)
                    <li{{ Request::is('login') ? ' class="active"' : null }}><a href="{{url('login')}}">
                            <i class="fa fa-user"></i> Login</a>
                    </li>
                @else
                    <li>
                        <a href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-btc"></i> {{$coinBalance}} &#36;</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li{{ Request::is('account') ? ' class="active"' : null }}>
                                <a href="{{route('account')}}">My Profile</a></li>
                            <li><a href="{{route('wallet')}}">My Wallet <i class="fa fa-btc"></i></a></li>
                            <li><a href="{{route('showDashboard')}}">Dashboard</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('showChat')}}">Chat</a></li>
                            <li><a href="#">Verify Identity</a></li>
                            <li><a href="{{route('showTwoFactorHelp')}}">2 Factor Authentication</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Public Profile</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{url('/logout')}}">Logout</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

