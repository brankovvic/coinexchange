@if ($errors->any())
    <div class="notifications alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        @if ($errors->first(0, ':message'))
            {{ $errors->first(0, ':message') }}
        @else
            Please check the form below for errors
        @endif
    </div>
@endif

@if (session('message'))
    <div class="notifications alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        {{ session('message') }}
    </div>
@endif
