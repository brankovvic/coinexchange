<div class="footer">
    <section id="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h4>About Us</h4>

                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                        Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>

                    <p>Pellentesque habitant morbi tristique senectus.</p>
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <h4>Company</h4>

                    <div>
                        <ul class="arrow">
                            <li><a href="#">The Company</a></li>
                            <li><a href="#">Our Team</a></li>
                            <li><a href="#">Our Partners</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Faq</a></li>
                            <li><a href="#">Conatct Us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Copyright</a></li>
                        </ul>
                    </div>
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <h4>Latest Blog</h4>

                    <div>
                        <div class="media">
                            <div class="media-body">
                                <span class="media-heading"><a href="#">Pellentesque habitant morbi tristique
                                        senectus</a></span>
                                <small class="muted">Posted 17 Aug 2013</small>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <span class="media-heading"><a href="#">Pellentesque habitant morbi tristique
                                        senectus</a></span>
                                <small class="muted">Posted 13 Sep 2013</small>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <span class="media-heading"><a href="#">Pellentesque habitant morbi tristique
                                        senectus</a></span>
                                <small class="muted">Posted 11 Jul 2013</small>
                            </div>
                        </div>
                    </div>
                </div><!--/.col-md-3-->


                <div class="col-md-3 col-sm-6">
                    <h4>Address</h4>
                    <address>
                        <strong>Twitter, Inc.</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        <abbr title="Phone">P:</abbr> (123) 456-7890
                    </address>
                    <ul class="nav nav-pills footer__social">
                        <li><a href="#"><i class="fa fa-legal fa-lg"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                    </ul>
                    <p>
                        &nbsp;
                    </p>
                    <h4>Newsletter</h4>

                    <form role="form">
                        <div class="input-group">
                            <input type="text" class="form-control" autocomplete="off" placeholder="Enter your email">
									<span class="input-group-btn">
										<button class="btn btn-danger" type="button">Go!</button>
									</span>
                        </div>
                    </form>
                </div> <!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->
</div>