<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" sizes="16x16 32x32" type="image/x-icon" href="{{asset('favicon-btc.png')}}">
    <title>
        BTC-Exchange @yield('title')
    </title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.4/simplex/bootstrap.min.css" class="bootswatch-css"
          rel="stylesheet" media="screen">
    <link type="text/css" href=" {{ asset('assets/css/jquery-ui.min.css') }}" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/normalize.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/ns-default.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/ns-style-growl.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/pnotify.custom.min.css')}}" rel="stylesheet">
    <style>
        .pac-container {
            z-index: 10000 !important;
        }
    </style>

@yield('styles')

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('assets/js/ie10-viewport-bug-workaround.js') }}"></script>
    <script type="text/javascript" src='//maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @if(auth()->check())
        <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
        <script>
            var socket = io("{{env('SOCKET_ADDRESS')}}", {query:"userId={{auth()->user()->getAttribute('id')}}"});
            socket.on("newNotification", function (message) {
                var notice = new PNotify({
                    title: 'New Trade Request',
                    text: 'Congratulations You Have New Trade Request!',
                    icon: 'glyphicon glyphicon-envelope',
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function() {
                    notice.remove();
                });
            });
        </script>
    @endif
</head>

<body>

<!-- Navigation -->
@include('layouts.includes.navigation')


@yield('header')

<!-- Layout -->
<div class="base">

    <!-- Notifications -->
    <div class="container">
        @include('layouts.includes.notifications')
    </div>

    <!-- Page -->
    <div class="page container">
        @yield('page')
    </div>

</div>


<!-- Footer -->
@include('layouts.includes.footer')

<!-- Model -->
<div id="us6-dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select Your Location</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" style="width: 550px">
                    <form action="ajax/user_locale" method="get" id="setlocation">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Location:</label>

                            <div class="col-sm-10"><input type="text" name="address" class="form-control"
                                                          id="us6-address"/></div>
                        </div>
                        <input type="hidden" name="city" id="us6-city">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Radius:</label>

                            <div class="col-sm-5"><input type="text" name='radius' class="form-control"
                                                         id="us6-radius"/></div>
                        </div>
                        <div id="us6" style="width: 100%; height: 400px;"></div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="output"></div>
                        <div class="m-t-small">
                            <div class="col-sm-3"><input type="hidden" name="lat" class="form-control"
                                                         style="width: 110px"
                                                         id="us6-lat"/></div>
                            <div class="col-sm-3"><input type="hidden" name="long" class="form-control"
                                                         style="width: 110px"
                                                         id="us6-lon"/></div>
                        </div>
                        <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Core scripts -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.validator.min.js') }}"></script>
<script src="{{ asset('assets/js/retina.min.js') }}"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/jquery.geocomplete.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-locationpicker.js') }}"></script>
<script src="{{ asset('assets/js/jquery.form.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{asset('assets/js/notifications/modernizr.custom.js')}}"></script>
<script src="{{asset('assets/js/notifications/classie.js')}}"></script>
<script src="{{asset('assets/js/notifications/notificationFx.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.min.js')}}" type="text/javascript"></script>
<!-- notifications plugin -->
<script src="{{asset('assets/js/pnotify.custom.min.js')}}"></script>
<!--Start of Zopim Live Chat Script-->
<!--End of Zopim Live Chat Script-->
<script>
    $(document).ready(function () {
        $('#us6').locationpicker({
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us6-lat'),
                longitudeInput: $('#us6-lon'),
                radiusInput: $('#us6-radius'),
                locationNameInput: $('#us6-address')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                console.log($(this).locationpicker('map'));
            }
        });
        $('#us6-dialog').on('shown.bs.modal', function () {
            $('#us6').locationpicker('autosize');
        });


    });
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({
            placement: 'right'
        });
    });
    // pre-submit callback
    function showRequest(formData, jqForm, options) {
        var queryString = $.param(formData);
        return true;
    }

    // post-submit callback
    function showResponse(responseText, statusText, xhr, $form) {
        $('#us6-dialog').modal('hide')
    }

    function showNotification(message) {
        var notification = new NotificationFx({
            message: message,
            layout: 'growl',
            effect: 'scale',
            type: 'notice', // notice, warning, error or success
            onClose: function () {
                bttn.disabled = false;
            }
        });
        // show the notification
        notification.show();
    }

</script>
@yield('scripts')

</body>
</html>
