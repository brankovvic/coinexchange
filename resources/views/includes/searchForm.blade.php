<form method="get" action="#" class="form-inline col-md-12" id="searchForm"
      style="margin:0 auto">
    <div class="col-sm-8 col-sm-offset-3 col-md-8 col-md-offset-3">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-btn">
                    <select name="searchType" class="selectpicker show-tick" style="height:45px !important; ">
                        <option value="3">I want to buy bitcoin</option>
                        <option value="4">I want to sell bitcoin</option>
                    </select>
                </div><!-- /btn-group -->
                <input type="text" name="minAmount" class="form-control" aria-label="..."
                       placeholder="Amount of BTC">
            </div><!-- /input-group -->
        </div>
        <div class="form-group input-group">
            <input type="text" name="location" id="location" class="form-control" id="exampleInputPassword3"
                   placeholder="Location">
            <input type="hidden" name="lat" id="lat" class="form-control">
            <input type="hidden" name="lng" id="lng" class="form-control">

            <div class="input-group-btn">
                <button type="submit" class="btn btn-default">Search</button>
            </div>
        </div>
    </div>
</form>