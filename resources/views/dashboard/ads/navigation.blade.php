<div class="row">
    <div class="col-md-12">
        <h1>Dashboard  <br> <small>In these pages you can view and manage your current advertisements and contacts.</small></h1>

        <ul class="nav nav-pills">
            <li class="user-panel-dd">
                <a href="{{route('wallet')}}">Wallet: 0 BTC</a></li>
            <li>
                <a href="#">Dashboard / Active</a>
            </li>

            <li><a href="#">Closed contacts</a></li>
            <li><a href="#">Released contacts</a></li>
            <li><a href="#">Cancelled contacts</a></li>
            <li><a href="#">Coupons</a></li>
        </ul>

    </div>
</div>