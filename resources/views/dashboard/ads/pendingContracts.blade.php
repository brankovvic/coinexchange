<h1>Your Pending Contact Requests</h1>
<table class="table table-bordered">
    <thead>
    <th class="col-lg-1">#</th>
    <th class="col-lg-1">Seller</th>
    <th class="col-lg-1">Amount</th>
    <th class="col-lg-1">Amount BTC</th>
    <th class="col-lg-1">Status</th>
    <th class="col-lg-1">Action</th>
    <th class="col-lg-1">Created At</th>
    </thead>
    <tbody>
    @foreach($tradeContractRequests as $tradeContractRequest)
        <tr>
            <td>{{$tradeContractRequest->getAttribute('id')}}</td>
            <td>{{$tradeContractRequest->ad->user->getAttribute('username')}}</td>
            <td>{{$tradeContractRequest->getAttribute('price')}}</td>
            <td>{{$tradeContractRequest->getAttribute('coinPrice')}} <i class="fa fa-btc"></i></td>
            <td>Waiting For Approval</td>
            <td>
                <button class="btn btn-danger">Cancel Request</button>
            </td>
            <td>{{$tradeContractRequest->getAttribute('created_at')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>