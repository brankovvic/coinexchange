<h1>Your Active Contacts</h1>
<table class="table table-bordered">
    <thead>
    <th class="col-lg-1">#</th>
    <th class="col-lg-2">Buyer</th>
    <th class="col-lg-1">Amount</th>
    <th class="col-lg-1">Amount BTC</th>
    <th class="col-lg-1">Status</th>
    <th class="col-lg-1">Created At</th>
    </thead>
    <tbody>
    @foreach($tradesContracts as $tradesContract)
        <tr>
            <td>{{$tradesContract->getAttribute('id')}}</td>
            <td>{{$tradesContract->ad->user->getAttribute('username')}}</td>
            <td>{{$tradesContract->getAttribute('price')}}</td>
            <td>{{$tradesContract->getAttribute('coinPrice')}} <i class="fa fa-btc"></i></td>
            <td>
                <form method="post" action="{{route('approveContract')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="contractId" value="{{$tradesContract->getAttribute('id')}}">
                    <button class="btn btn-primary">Approve Payment</button>
                </form>
            </td>
            <td>{{$tradesContract->getAttribute('created_at')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>