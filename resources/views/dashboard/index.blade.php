@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Dashboard')

@section('page')
    <section class="users">
        @include('dashboard.ads.pendingContracts')
        @include('dashboard.ads.active')
        @include('dashboard.ads.navigation')
        Page 1 of 5
        <div class="pull-right">
            Render
        </div>
        <br/><br/>
        <table class="table table-bordered">
            <thead>
            <th class="col-lg-1">#</th>
            <th class="col-lg-1">Status</th>
            <th class="col-lg-1">Price/Per Btc</th>
            <th class="col-lg-2">Info</th>
            <th class="col-lg-1">Min Amount</th>
            <th class="col-lg-1">Max Amount</th>
            <th class="col-lg-2">Created</th>
            <th class="col-lg-1">Actions</th>
            </thead>
            <tbody>
            @foreach($ads as $ad)
                <tr>
                    <td>{{$ad->getAttribute('id')}}</td>
                    @if($ad->getAttribute('status') == "Active")
                        <td><a href="#">Deactivate</a></td>
                    @else
                        <td><a href="#">Activate</a></td>
                    @endif
                    <td>{{$ad->getAttribute('price')}}</td>
                    <td>{{$ad->getAttribute('description')}}</td>
                    <td>{{$ad->getAttribute('min_amount')}}</td>
                    <td>{{$ad->getAttribute('max_amount')}}</td>
                    <td>{{$ad->getAttribute('created_at')}}</td>
                    <td>
                        <form method="get"
                              action="#">
                            <button class="btn btn-sm btn-danger pull-left">Edit</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        Page 1 of 2
        <div class="pull-right">
            {{ $ads->links() }}
        </div>
    </section>
@stop
