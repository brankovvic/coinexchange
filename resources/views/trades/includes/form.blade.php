<form action="{{route('createNewTrade', $ad->getAttribute('id'))}}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-md-6 span amount">
        <div class="input-group{{ $errors->has('price') ? ' has-error' : '' }}">
            <span class="input-group-addon">USD</span>
            <input class="form-control" id="amountinput" step="any" name="price"
                   title="Remember to suggest trade sum amount between limit" min="1"
                   type="number"/>
            @if ($errors->has('price'))
                <span class="help-block">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-md-6 span btc">
        <div class="input-group {{ $errors->has('coinPrice') ? ' has-error' : '' }}">
            <span class="input-group-addon" id="basic-addon1">BTC</span>
            <input class="form-control" id="btcinput" min="{{$ad->min_amount}}" max="{{$ad->max_amount}}" step="any"
                   name="coinPrice" type="number"/>
            @if ($errors->has('coinPrice'))
                <span class="help-block">
                    <strong>{{ $errors->first('coinPrice') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <p>
        &nbsp;
    </p>
    <center>
        <button type="submit" class="btn btn-success btn-lg ">Create Request</button>
    </center>
</form>
