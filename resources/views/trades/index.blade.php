@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Trade')

{{-- Page content --}}
@section('page')

    <div class="container">
        <div class="col-md-6">
            <h2>Trade Information</h2>

            <div class="row">
                <div class="col-md-4">
                    <strong>Price</strong>
                </div>
                <div class="col-md-6">
                    {{$ad->getAttribute('price')}} USD
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-4">
                    <strong>Payment Details</strong>
                </div>
                <div class="col-md-6">
                    Payment Details
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-4">
                    <strong>User:</strong>
                </div>
                <div class="col-md-6">
                    <i class="fa fa-user"></i> {{$ad->user->username}} &nbsp; now<br/>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-4">
                    <strong>Trade Limits : </strong>
                </div>
                <div class="col-md-4">
                    {{$ad->min_amount}} - {{$ad->max_amount}} USD
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-4">
                    <strong>Payment Window : </strong>
                </div>
                <div class="col-md-4">
                    90 Minutes
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-4">
                    <strong>Location :</strong>
                </div>
                <div class="col-md-6">
                    {{ $userLocation }}
                </div>
            </div>
            <p>&nbsp;</p>
            <div id="amountdiv" class="row">
                @include('trades.includes.form')
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>

        <div class="col-md-6">
            <h2>Test Terms</h2>
            <pre>Other Info</pre>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne">
                                How to begin and contact the trader
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            Read the the terms of the trade and make sure you can comply with them. Use this form to
                            send in a trade request with the sum you wish to trade. After opening the trade request it
                            is possible to discuss with the trader in LocalBitcoins.com messaging system.
                            Open trade requests and message inbox can be found under Dashboard under your user profile
                            page. You can send and receive messages with the trader there.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                How to pay online
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            After sending the trade request you get the payment details.

                            Trader may not publish the payment details directly and asks you to contact to get the exact
                            account name needed for the payment. In this case, send a LocalBitcoins.com message to the
                            the trader and ask for the further details.

                            When buying bitcoins online, the payment window is 90 minutes, but this may vary depending
                            on the payment method and the terms of the trade.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Cancelling the trade
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            You can cancel the trade before making the payment. You find open trades under Dashboard in
                            your user profile.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="ad-tips">
        <div class="col-md-4">
            <div class="well">
                <h4><i class="fa fa-question"></i> How to proceed</h4>

                <p>Submit the trade request form, then fund the trade with your bitcoins, enable the escrow and wait for
                    the buyer to make the payment. The buyer has 90 Minutes time to make the
                    payment. When you have received the payment, release the bitcoins to the buyer.
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div id="ad-tips" class="well">
                <h4><i class="fa fa-info-circle"></i> Tips</h4>
                <ul>
                    <li>Read the ad, and check the terms.</li>
                    <li>Propose a meeting place and time when you contact, if physical cash is traded.</li>
                    <li>Watch for fraudsters! Check the profile feedback, and take extra caution with recently created
                        accounts.
                    </li>
                    <li>Note that rounding and price fluctuation might change the final bitcoin amount. The fiat amount
                        you enter matters and bitcoin amount is calculated at the time of request.
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        var price = "{{$ad->getAttribute('price')}}"
        $("#btcinput").on('input', function(e) {
            $('#amountinput').val($(this).val() * price);
        });

        $("#amountinput").on('input', function(e) {
            $('#btcinput').val($(this).val() / price);
        });
    </script>
@stop
