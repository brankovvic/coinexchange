@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Wallet')

{{-- Page content --}}
@section('page')
    <div class="col-md-12">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Send BTC</h3>
                </div>
                <div class="panel-body">
                    <p>Your wallet balance: {{$amount}}</p>

                    <div class="col-md-11">
                        @include('wallet.form')
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Recieve BTC</div>
                <div class="panel-body">
                    <p>Give out the bitcoin address below to receive bitcoins. </p>
                    <center>
                        <p class="center-block" id="btcaddress"><strong>{{$address}}</strong>
                            <br/>
                            <small>Press CTRL + C or &#x2318; + C to copy the bitcoin address.
                            </small>
                        </p>
                    </center>
                    <!--<p><strong>Unconfirmed Balance : </strong> {{$amount}}</p>-->

                    <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="well2-headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#well2-collapseOne"
                                       aria-expanded="true" aria-controls="well2-collapseOne">
                                        <i class="fa fa-chevron-down"></i>&nbsp;Incoming transactions
                                    </a>
                                </h4>
                            </div>
                            <div id="well2-collapseOne" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="well2-headingOne">
                                <div class="panel-body">
                                    <p>We credit the incoming transaction after three
                                        <a target="_blank" href="https://en.bitcoin.it/wiki/Confirmation">bitcoin
                                            network confirmations</a>.
                                        Usually receiving takes couple of minutes depending on the bitcoin network
                                        state. Currently minimum deposit amount is 0.0002 BTC. Bitcoin deposits less
                                        than that are not processed.
                                        <a target="_blank" href="#confirm_transaction">More information about Bitcoin
                                            transactions</a>.</p>

                                    <p><a target="_blank" class="btn btn-default"
                                          href="https://blockchain.info/address/{{$address}}">
                                            <i class="fa fa-search-plus"></i> Show your incoming Bitcoin transactions
                                        </a></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="well2-headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3"
                                       href="#well2-collapseTwo" aria-expanded="false"
                                       aria-controls="well2-collapseTwo">
                                        <i class="fa fa-chevron-down"></i>&nbsp;QR code for mobile
                                    </a>
                                </h4>
                            </div>
                            <div id="well2-collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="well2-headingTwo">
                                <div class="panel-body">
                                    <p>This is your receiving address as a QR code. It is possible to send bitcoins to
                                        you from mobile wallets by scanning this code.</p>
                                    <img id="wallet-qr-code"
                                         src="http://www.btcfrog.com/qr/bitcoinPNG.php?address={{$address}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="well2-headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3"
                                       href="#well2-collapseThree" aria-expanded="false"
                                       aria-controls="well2-collapseThree">
                                        <i class="fa fa-chevron-down"></i>&nbsp;Manage addresses
                                    </a>
                                </h4>
                            </div>
                            <div id="well2-collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>

                                    <p>To protect your privacy, we provide a new bitcoin address for each transfer. This
                                        prevents someone from tracking all the payments you receive by looking at the
                                        blockchain. Note that any address provided here will continue to be linked to
                                        your account.</p>
                                    </p>


                                    <h4>Old addresses</h4>
                                    <table class="table table-striped table-condensed">
                                        <tr>
                                            <th>Created</th>
                                            <th>Address</th>
                                        </tr>
                                        <tr>
                                            <td>02/02/2017</td>
                                            <td>
                                                <small>
                                                    <a target="_blank"
                                                       href="https://blockchain.info/address/2MuUKN8pQBj1XsNx3DzoxyHmhSpn2TdPX27">
                                                        2MuUKN8pQBj1XsNx3DzoxyHmhSpn2TdPX27
                                                    </a>
                                                </small>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
<script>
    var fee = {{$fee}}
    $('#coinAmount').on('input', function() {
        $(this).val($(this).val() - fee);
    })
</script>
@stop
