<form action="{{route('transferCoin')}}" method="post" class="btc_send form-horizontal validate-form"
      data-bv-message="This value is not valid"
      data-bv-feedbackicons-valid="fa fa-check-sqaure-o fa-lg"
      data-bv-feedbackicons-invalid="fa fa-warning"
      data-bv-feedbackicons-validating="fa fa-refresh">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
        <label>Recieving Bitcoin Address</label>
        <input class="form-control" name="address" type="text" placeholder="Bitcoin Address" required autofocus
               data-bv-emailaddress-message="Input valid bitcoin address"/>
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('coinAmount') ? ' has-error' : '' }}">
        <label>Amount in bitcoins</label>
        <input class="form-control" min="0" max="{{$amount}}" step="0.0001" name="coinAmount" type="number"
               placeholder="0.0000" required id="coinAmount" data-bv-password-message="Amount is required"/>
        @if ($errors->has('coinAmount'))
            <span class="help-block">
                <strong>{{ $errors->first('coinAmount') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-btc"></i> Send BTC
        </button>
    </div>

</form>