<div class="form-group" id="locate1">
    <div class="col-md-6">
        <label>Location <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i></label>
        <input type="text" value="{{$userLocation}}" class="form-control" id="location1" required>
        <input name="lat" type="hidden" value="{{$lat}}">
        <input name="lon" type="hidden" value="{{$lon}}">
    </div>
    <div class="col-md-6">
    </div>
</div>
<div class="form-group" >
    <div class="col-md-6">
        <label>Select Your Currency <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i></label>
        <select class="form-control" name="currencyId" id="currency"  onchange="currency_update(this);" >
            <option>---</option>
            @foreach ($countries as $cur)
                @if($cur->currency_code === 'USD')
                    <option id={{$cur->iso_3166_2}} value="{{ $cur->id }}" selected>{{ $cur->currency_code }}</option>
                @else
                    <option id={{$cur->iso_3166_2}} value="{{ $cur->id }}" >{{ $cur->currency_code }}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="col-md-6">

    </div>
</div>