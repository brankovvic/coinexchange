<div class="clearfix"></div>
<div class="form-group">
    <h2>More Information</h2>
    <hr>
    <div class="col-md-3">
        <label>Price/Per Btc <i class="fa fa-info-circle" data-container="body" data-toggle="popover"
                        data-placement="top"
                        data-content="Price Per BitCoin"></i></label>

        <div class="input-group{{ $errors->has('price') ? ' has-error' : null }}">
            <input type="number" name="price" class="form-control">
            <span class="input-group-addon" id="price">USD</span>
        </div>
        @if ($errors->has('price'))
            <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="col-md-3">
        <label>Min. Amount <i class="fa fa-info-circle" data-container="body" data-toggle="popover"
                              data-placement="top"
                              data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i></label>

        <div class="input-group{{ $errors->has('name') ? ' has-error' : null }}">
            <input type="text" name="min_amount" class="form-control" min="0">
            <span class="input-group-addon" id="curr1">BTC</span>
        </div>
        @if ($errors->has('min_amount'))
            <span class="help-block">
                                        <strong>{{ $errors->first('min_amount') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('max_amount') ? ' has-error' : null }}">
        <div class="col-md-3">
            <label>Max. Amount <i class="fa fa-info-circle" data-container="body" data-toggle="popover"
                                  data-placement="top"
                                  data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i></label>

            <div class="input-group">
                <input type="number" name="max_amount" class="form-control">
                <span class="input-group-addon" id="curr2">BTC</span>
            </div>
            @if ($errors->has('max_amount'))
                <span class="help-block">
                                        <strong>{{ $errors->first('max_amount') }}</strong>
                                    </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('description') ? ' has-error' : null }}">
        <div class="col-md-8">
            <label>Other Info <i class="fa fa-info-circle" data-container="body" data-toggle="popover"
                                 data-placement="top"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i></label>
            <textarea name="description" class="form-control" rows="6"></textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif
        </div>
    </div>
</div>
<div class="clearfix"></div>
<h2>Security options</h2>
<hr>
<div class="form-group{{ $errors->has('description') ? ' has-error' : null }}">
    <label>
        <input type="radio" name="authy_verified" value="0"> Mobile Number Verified
    </label> <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="top"
                data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i>
    @if ($errors->has('authy_verified'))
        <span class="help-block">
                                        <strong>{{ $errors->first('authy_verified') }}</strong>
                                    </span>
    @endif
</div>
<button type="submit" name="submit" id="submit" value="buylocal" class="btn btn-success">
    Advertise
</button>