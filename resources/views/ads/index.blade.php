@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Create Advertisment')

{{-- Page content --}}
@section('page')
    <section class="ads">
        <header class="page-header">
            <h1>Advertise</h1>
        </header>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation">
                    <a href="#coinBuy" id="buyonline" aria-controls="coinTrade" role="tab"
                                           data-toggle="tab">Buy BTC</a></li>
                <li role="presentation">
                    <a href="#coinBuy" id="sellonline" aria-controls="coinTrade" role="tab"
                                           data-toggle="tab">Sell BTC</a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="coinBuy">
                    <form action="{{route('createTrade')}}" method="post" autocomplete="off" id="coinTrade">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="tradeType" name="trade_type" value="1">
                        @include('ads.includes.location')
                        @include('ads.includes.mainTemplate')
                    </form>
                </div>
            </div>
        </div>
    </section>

@section('scripts')
    <script src="{{ asset('assets/js/deps.js') }}"></script>

    <script>
        $('#buyonline').onclick(function() {
            document.getElementById('tradeType').value = "{{config('trade.buy')}}";
        });
        $('#buyonline').onclick(function() {
            document.getElementById('tradeType').value = "{{config('trade.sell')}}";
        });
        $(document).ready(function () {

            $("#location1").geocomplete({details: "form"}).bind("geocode:result", function (event, result) {

                var countryCode = getCountryCode(result);
                console.log(countryCode);
                // search the payment and add to payment container
                document.getElementById(countryCode).selected = true;
            });

            $('#buylocallymain').show();
            function currency_update(sel) {
                $('#curr1').html(sel.value);
                $('#curr2').html(sel.value);
            }
            function getCountryCode(data) {
                var locations = data.address_components;
                var strCountry = 'country';
                for (var prop in locations) {
                    if (locations.hasOwnProperty(prop)) {
                        var location = locations[prop];

                        if ($.inArray(strCountry, location.types) != -1) {
                            return location.short_name;
                        }

                    }
                }

            }

        });
    </script>
@stop

@stop