@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Buy')

@section('styles')
    <link href="{{ asset('assets/css/viewgrid.css') }}" rel="stylesheet">
@stop

{{-- Page content --}}
@section('page')
    <section>
        <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
            <div class="cbp-vm-options">
                <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid">Grid View</a>
                <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list">List View</a>
            </div>
            <ul>
                @foreach ($ads as $ad)
                    <li>
                        <h3 class="cbp-vm-title">{{$ad->user->username}} &nbsp; now (15;100%)</h3>
                        <div class="cbp-vm-price">{{$ad->price}} USD</div>
                        <div class="cbp-vm-details">
                            payment details

                        </div>
                        <a class="cbp-vm-icon cbp-vm-add" href="{{route('showCreateTrade', $ad->getAttribute('id'))}}">Buy
                            Now</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{ asset('assets/js/classie.js') }}"></script>
    <script src="{{ asset('assets/js/gridview.js') }}"></script>
@stop