@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Home')

{{-- Inline styles --}}
@section('styles')
    <link href="{{ asset('assets/css/welcome.css') }}" rel="stylesheet">
    <style>
        /* CUSTOMIZE THE CAROUSEL
        -------------------------------------------------- */

        /* Carousel base class */
        .carousel {
            height: 500px;
            margin-bottom: 60px;
        }

        /* Since positioning the image, we need to help out the caption */
        .carousel-caption {
            z-index: 10;
        }

        /* Declare heights because of positioning of img element */
        .carousel .item {
            height: 500px;
            background-color: #777;
        }

        .carousel-inner > .item > img {
            position: absolute;
            top: 0;
            left: 0;
            min-width: 100%;
            height: 500px;
        }

        #searchForm {
            position: absolute;
            top: 35%;
        }

        #change-location {
            color: red;
        }

        #change-location:hover {
            cursor: pointer;
        }


    </style>
@stop

@section('header')
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide"
                     src="http://www.info-compensation-carbone.com/wp-content/uploads/2016/07/photo-2.jpg"
                     alt="First slide">

                <div class="container">
                    <div class="carousel-caption">
                        <h1>Buy And Sell Bitcoins Online</h1>
                        <p>Instant. Secure. Private.</p>
                        <p><a class="btn btn-lg btn-primary" href="{{url('login')}}" role="button">Sign up today</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="second-slide"
                     src="http://s3.amazonaws.com/mnb_keystone/Bitcoin_the_rise_of_cryptocurrency_dark.jpg"
                     alt="Second slide">

                <div class="container">
                    <div class="carousel-caption">
                        <h1>Another example headline.</h1>

                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="third-slide"
                     src="https://dcebrief.com/wp-content/uploads/2015/10/digital_currency.jpg"
                     alt="Third slide">

                <div class="container">
                    <div class="carousel-caption">
                        <h1>One more for good measure.</h1>

                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Search Form -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div><!-- /.carousel -->
@stop

{{-- Page content --}}

@section('page')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Heading</h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor
                    mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna
                    mollis euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Heading</h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor
                    mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna
                    mollis euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Heading</h2>

                <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula
                    porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
                    ut fermentum massa justo sit amet risus.</p>

                <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
            </div>
        </div>
        <div class="page-header">
            <h1>Buy Bitcoins near {{$userLocation}}
                <small>in USD</small>
            </h1>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Seller</th>
                <th>Description</th>
                <th>Price/BTC</th>
                <th>Limits</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($buyAds as $ad)
                <tr>
                    <td>{{$ad->user->getAttribute('username')}} &nbsp; 1 (15; 100%)</td>
                    <td>{{$ad->getAttribute('description')}}</td>
                    <td>{{$ad->getAttribute('price')}}</td>
                    <td>{{$ad->max_amount}}BTC - {{$ad->min_amount}} BTC</td>
                    <td align="center"><a class="btn btn-success" href="#">Sell Now</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('scripts')
    <script>
        $(window).ready(function () {
            $('#change-location-form').hide();
            $("#location").geocomplete({details: "form"}).bind("geocode:result", function (event, result) {
                $("#location").val(result.formatted_address);
                data = {};
                data['lat'] = $('#lat').val();
                data['long'] = $('#lng').val();
                data['address'] = result.formatted_address;
                $.ajax({
                    type: "POST",
                    url: "#",
                    data: data
                });
            });

            $("#searchForm").submit(function () {
                ('#lat').remove()
                ('#lat').remove()
            });
            $("#id_place").geocomplete({details: "form"}).bind("geocode:result", function (event, result) {
                $('#change-location-form').submit()
            });
        });
        $('#change-location').click(function (event) {
            $('#change-location-form').show();
        })
    </script>
@stop