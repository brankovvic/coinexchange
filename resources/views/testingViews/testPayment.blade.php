@extends('layouts.master')

{{-- Page title --}}
@section('title', 'Create Advertisment')

{{-- Page content --}}
@section('page')
    <form action="{{route('payPalDeposit')}}" method="post">
        {{csrf_field()}}
        <div class="from-group">
            <label>Amount To Be Deposit</label>
            <input type="text" name="amount" value="{{old('amount')}}"/>
        </div>
        <div class="from-group">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
@stop